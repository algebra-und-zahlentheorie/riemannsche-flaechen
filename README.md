# Riemannsche Flächen

Hier ist Material zur Vorlesung 'Riemannsche Flächen' an der Universität
Augsburg im Wintersemester 2018/2019 zu finden.
Die
[Übungsblätter](http://algebra-und-zahlentheorie.gitlab.io/riemannsche-flaechen/uebungen.pdf)
zur Vorlesung stehen als pdf zum Download bereit.
