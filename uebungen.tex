\documentclass[a4paper,oneside,12pt]{report}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{german}
\usepackage{hyperref}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsxtra}
\usepackage{amsthm}
\usepackage{amscd}
\usepackage{mathtools}
\usepackage{fancyhdr}

\theoremstyle{definition}
\newtheorem{ex}{Aufgabe}

\renewcommand\chaptername{Übung}
\renewcommand\theenumi{\alph{enumi}}
\renewcommand\labelenumi{(\theenumi)}
\setcounter{chapter}{-1}
\setcounter{ex}{-4}

\pagestyle{plain}
\fancypagestyle{plain}{\fancyhf{}%
  \setlength\headheight{43pt}%
  \rhead{Wintersemester 2018/2019\\ Riemannsche Flächen}%
  \lhead{Prof.~Dr.~Marc Nieper-Wißkirchen\\ Caren Schinko, M.~Sc.}%
  \cfoot{\thepage}
  \renewcommand\headrulewidth{0pt}}

\let\epsilon\varepsilon
\let\theta\vartheta
\let\phi\varphi

\begin{document}

\chapter{Auftakt}

\begin{ex}[m]
  Gibt es eine holomorphe Funktion $f \colon \mathbf E \to \mathbf C$ auf der
  offenen Einheitskreisscheibe $\mathbf E \subseteq \mathbf C$, welche die
  Bedingung
  \[
  f \Big(\frac{1}{2n}\Big) = f \Big(\frac{1}{2n+1}\Big) = \frac 1 n
  \]
  für alle $n \in \mathbf N_1$ erfüllt?
\end{ex}

\begin{ex}[m]
  Sei $z_0$ Nullstelle der holomorphen Funktion $f$ von Ordnung $n$. Zeigen Sie,
  daß es genau dann eine in einer Umgebung von $z_0$ holomorphe Funktion $h$
  mit $(h(z))^k = f(z)$ gibt, wenn $k$ die Ordnung $n$ teilt.
\end{ex}

\begin{ex}[m]
  Seien $X$ ein wegweise zusammenhängender topologischer Raum und
  $I \coloneqq [0,1]$. Eine Weg $u$ in $X$ ist eine stetige Abbildung
  $u \colon I \to X$. Zeigen Sie, dass folgende Aussagen äquivalent sind:
  \begin{enumerate}
  \item
    $X$ ist einfach zusammenhängend, d.h. jeder geschlossene Weg $u$ ist
    homotop relativ seiner Endpunkte
    zum konstanten Weg $\underline{a}\colon I \to X$ für $a \coloneqq
    u(0) = u(1)$.
  \item
    Jede stetige Abbildung $u \colon \mathbf S^1 \to X$ kann zu einer stetigen
    Abbildung
    $\overline{u}\colon \mathbf D \to X$ fortgesetzt werden, wobei $\mathbf D
    \coloneqq \{ z \in \mathbf C | \left|z\right| \leq 1 \}$.
  \item
    Sind $u,v$ Wege mit $u(0)=v(0)$ und $u(1)=v(1)$, so sind $u$ und $v$
    homotop relativ ihrer Endpunkte.
  \end{enumerate}
\end{ex}

\begin{ex}[m]
  Die Funktion $f_0$ ist durch die Potenzreihe
  \[
  \sum_{n=1}^\infty \frac{(-1)^{n-1}}{n(2n-1)}z^{2n}
  \]
  auf der Einheitskreisscheibe $\mathbf E$  definiert. Setzen Sie die Funktion
  längs jeden möglichen Weges analytisch fort. Genauer gesagt: Finden Sie eine
  passende holomorphe Funktion auf einem passenden Definitionsbereich. Rufen Sie
  sich dabei ins Gedächtnis: Wieso gibt es eine solche Funktion und wieso sind
  manche Wege nicht möglich?
\end{ex}

\chapter{Definition Riemannscher Flächen}

\begin{ex}
  Ein lokal kompakter Hausdorffraum $X$ ist ein Hausdorffraum $X$, so daß
  alle Punkte $p \in X$ eine kompakte Umgebung $K \subseteq X$ besitzen. Zum
  Beispiel ist $\mathbf R^n$ ein lokal kompakter Hausdorffraum, denn für $p \in X$
  ist zum Beispiel der Einheitsball um $p$ eine kompakte Umgebung von $p$.
  \begin{enumerate}
  \item(s)
    Sei $X$ ein lokal kompakter Hausdorffraum. Wir setzen
    \[
      \hat X \coloneqq X \amalg \{\infty\},
    \]
    das heißt $\hat X$ ist die Menge $X$ vereinigt mit einem weiteren Element
    $\infty$ mit $\infty \notin X$. Wir nennen eine Teilmenge $U \subseteq \hat X$
    \emph{in $\hat X$ offen}, falls
    \begin{enumerate}
    \item $\infty \notin U$ (d.h.~$U \subseteq X$) und $U$ offen in $X$ ist oder falls
    \item $\infty \in U$ und $\hat X \setminus U$ ein kompakter Teilraum von $X$ ist.
    \end{enumerate}
    Zeigen Sie, daß dadurch eine Topologie auf $\hat X$ definiert wird, die $\hat X$
    zu einem kompakten Hausdorffraum macht. (Wo benutzen Sie, daß $\hat X$ lokal
    kompakt ist?)

    Mit dieser Topologie heißt $\hat X$ die \emph{Ein-Punkt-Kompaktifizierung
    von $X$}.
  \item(m)
    Sei $n \in \mathbf N_0$ und $\widehat{\mathbf R^n}$ die Ein-Punkt-Kompaktifizierung
    des $\mathbf R^n$. Weiter sei
    \[
      \mathbf S^n \coloneqq \{(x^0, \dotsc, x^n) \in \mathbf R^{n + 1} \mid
      \sum_{i = 0}^n (x^i)^2 = 1\}
    \]
    die \emph{Einheits-$n$-Sphäre}. Wir definieren die \emph{stereographische
    Projektion} durch
    \[
      \sigma\colon \mathbf S^n \to \widehat{\mathbf R^n},
      (x^0, \dotsc, x^n) \mapsto \begin{cases}
        \frac 1{1 - x^n} \cdot (x^0, \dotsc, x^{n - 1}), & \text{falls $x^n \neq 1$ und} \\
        \infty, & \text{falls $x^n = 1$.}
      \end{cases}
    \]
    Zeigen Sie, daß $\sigma$ ein Homöomorphismus ist. (Können Sie $\sigma$ durch eine
    Zeichnung veranschaulichen?)

    Wir können also sagen, daß die $n$-Sphäre die Ein-Punkt-Kompaktifizie\-rung des
    $\mathbf R^n$ ist.
  \end{enumerate}
\end{ex}

\begin{ex}[s]
  Seien $\Gamma \coloneqq \mathbf Z \omega_1 + \mathbf Z \omega_2$ und
  $\Gamma' \coloneqq \mathbf Z \omega_1' + \mathbf Z \omega_2'$ zwei Gitter in
  $\mathbf C$. Zeigen Sie, daß $\Gamma = \Gamma'$ genau dann gilt, wenn eine
  Matrix $A \in \mathrm{GL}_2(\mathbf Z)$ mit
  \[
    \begin{pmatrix}
      \omega_1' \\ \omega_2'
    \end{pmatrix}
    =
    A \cdot \begin{pmatrix}
      \omega_1 \\ \omega_2
    \end{pmatrix}
  \]
  existiert.
\end{ex}

\begin{ex}
  \leavevmode
  \begin{enumerate}
  \item(s)
    Seien $\Gamma, \Gamma' \subseteq \mathbf C$ zwei Gitter.
    Sei $\alpha \in \mathbf C^\times$ mit $\alpha \Gamma \subseteq \Gamma'$.
    Zeigen Sie, daß die Abbildung $\mathbf C \to \mathbf C,\ z \mapsto \alpha z$
    eine holomorphe Abbildung
    \[
      \mathbf C/\Gamma \to \mathbf C/\Gamma'
    \]
    induziert, welche genau dann biholomorph ist, wenn $\alpha \Gamma = \Gamma'$.
  \item(m)      
    Zeigen Sie, daß jeder Torus $X = \mathbf C/\Gamma$ isomorph zu einem Torus der
    Form
    \[
      X(\tau) \coloneqq \mathbf C/(\mathbf Z + \mathbf Z \tau)
    \]
    mit
    \[
      \tau \in \mathbf H \coloneqq \{z \in \mathbf C \mid \Im(z) > 0\}
    \]
    ist.
  \item(m)
    Seien
    \[
      \begin{pmatrix} a & b \\ c & d \end{pmatrix}
      \in \mathrm{SL}_2(\mathbf Z)
    \]
    und $\tau \in \mathbf H$. Sei
    \[
      \tau' \coloneqq \frac{a \tau + b}{c \tau + d}.
    \]
    Zeigen Sie, daß die Tori $X(\tau)$ und $X(\tau')$ isomorph sind.
  \end{enumerate}
\end{ex}

\chapter{Holomorphe Abbildungen und Garben}

\begin{ex}[m]
  Sei eine invertierbare Matrix
  \[
    A = \begin{pmatrix} a & b \\ c & d\end{pmatrix} \in \mathrm{GL}_2(\mathbf C)
  \]
  gegeben. Zeigen Sie, daß die auf $\mathbf C$ meromorphe Funktion
  \[
    \phi(z) = \frac{a z + b}{c z + d},
  \]
  welche holomorph auf $\{c z + d \neq 0\}$ ist, eindeutig zu einer biholomorphen
  Funktion $\hat\phi\colon \mathbf P^1 \to \mathbf P^1$ fortgesetzt werden kann.
  (Schreibe $A \cdot z \coloneqq \hat\phi(z)$. Was ist dann $(AB) \cdot z$ und $I_2 \cdot z$
  für $A$, $B \in \mathrm{GL}_2(\mathbf C)$ und $I_2 = \left(\begin{smallmatrix} 1 & 0 \\
  0 & 1\end{smallmatrix}\right)$?)
\end{ex}

\begin{ex}[s]
  Sei
  \[
    \sigma\colon \mathbf S^2 \to \mathbf P^1 = \mathbf C \cup \{\infty\}
    = \mathbf R^2 \cup \{\infty\}
  \]
  die stereographische Projektion. Sei
  \[
    A \in \mathrm{SO}_3(\mathbf R) = \{A \in \mathrm{GL}_3(\mathbf R) \mid
    A^T A = I_3, \det A = 1\}
  \]
  eine orientierungserhaltene Drehung des $\mathbf R^3$. Zeigen Sie, daß die
  Abbildung
  \[
    \sigma \circ A \circ \sigma^{-1}\colon \mathbf P^1 \to \mathbf P^1
  \]
  biholomorph ist.
  (Überlegen Sie sich, daß die Gruppe $\mathrm{SO}_3(\mathbf R)$ von Matrizen
  der Form
  \[
    \begin{pmatrix} 0 & 1 & 0 \\ 0 & 0 & 1 \\ 1 & 0 & 0\end{pmatrix}\quad
    \text{und}
    \quad
    \begin{pmatrix} B & 0 \\ 0 & 1\end{pmatrix}
  \]
  mit $B \in \mathrm{SO}_2(\mathbf R)$
  erzeugt wird.)
\end{ex}

\begin{ex}[m]
  Sei $X$ eine Riemannsche Fläche. Für eine offene Teilmenge $U \subset X$ sei
  $\mathcal B(U)$ die $\mathbf C$-Algebra der beschränkten holomorphen Funktionen
  $f\colon U \to \mathbf C$. Für $V \subseteq U$, $V$ offen, sei $\mathcal B(U) \to \mathcal B(V)$ 
  die übliche Einschränkungsabbildung. Zeige, daß $\mathcal B$ eine
  separierte\footnote{Die Prägarbe $\mathcal B$ heißt \emph{separiert}, falls
    sie das erste Garbenaxiom erfüllt, wenn also für jede offene Teilmenge
    $U\subseteq X$ und für jede offene Überdeckung $\bigcup_{i\in I} U_i=U$ gilt
    \[
    f,g \in \mathcal B(U) \text{ mit } f\mid U_i = g\mid U_i \text{ für alle }
    i \in I \Rightarrow f=g.
    \]}
  Prägarbe, im allgemeinen aber keine Garbe ist.
\end{ex}

\begin{ex}[s]
  Sei $X$ eine Riemannsche Fläche. Für eine offene Teilmenge $U \subseteq X$ setzen wir
  \[
    \mathcal F(U) \coloneqq \mathcal O^\times(U)/\exp(\mathcal O(U)).
  \]
  Zeigen, daß $\mathcal F$ mit den üblichen Einschränkungsabbildungen eine im allgemeinen nicht 
  separierte Prägarbe ist.
\end{ex}

\begin{ex}[s]
  Sei $\mathcal F$ eine Prägarbe abelscher Gruppen auf dem topologischen Raum $X$,
  und sei $p\colon |\mathcal F| \to X$ der assoziierte Totalraum. Für eine offene Teilmenge
  $U \subseteq X$ sei $\tilde{\mathcal F}(U)$ die Menge aller \emph{Schnitte von $p$ über $U$},
  das heißt die Menge der stetigen Abbildungen
  \[
    f\colon U \to |\mathcal F|
  \]
  mit $p \circ f = \operatorname{id}_U$. Zeigen Sie, daß $\tilde{\mathcal F}$ mit den
  natürlichen Einschränkungsabbildungen eine Garbe ist und daß natürliche Isomorphismen
  \[
    \mathcal F_x \to \tilde{\mathcal F}_x
  \]
  zwischen den Halmen für alle $x \in X$ existieren.
\end{ex}

\chapter{Elementare Eigenschaften holomorpher Abbildungen}

\begin{ex}
  Sei $\Gamma \subseteq \mathbf C$ ein Gitter. Die \emph{Weierstraßsche $\wp$-Funktion} bezüglich
  $\Gamma$ 
  ist durch
  \[
    \wp_\Gamma(z) \coloneqq \frac 1 {z^2} + \sum_{\omega \in \Gamma \setminus 0} \left(\frac 1 {(z -     
    \omega)^2} - \frac 1 {\omega^2} \right)
  \]
  definiert.
  \begin{enumerate} 
  \item(s)
    Zeigen Sie, daß $\wp_\Gamma$ eine doppelt-periodische meromorphe Funktion bezüglich $\Gamma$ ist, 
    welche Pole an den Punkten von $\Gamma$ hat.
    
    (Betrachten Sie die Ableitung
    \[
      \wp'_\Gamma(z) = - 2 \sum_{\omega \in \Gamma} \frac 1 {(z - \omega)^3}.)
    \]
  \item(m)
    Sei $f \in \mathcal M(\mathbf C)$ eine doppelt-periodische, meromorphe Funktion bezüglich
    $\Gamma$,
    welche Pole an 
    den Punkten von $\Gamma$ hat und welche um den Ursprung eine Laurent-Reihenentwicklung der Form
    \[
      f(z) = \frac 1 {z^2} + \sum_{k = 1}^\infty c_k z^k
    \]
    hat.
    Zeigen Sie, daß $f = \wp_\Gamma$.
  \end{enumerate}
\end{ex}

\begin{ex}[s]
  Seien $X$ eine Riemannsche Fläche und $f\colon X \to \mathbf C$ eine nicht-konstante
  holomorphe Funktion. Zeigen Sie, daß ihr Realteil $\Re f$ nirgends sein Maximum annimmt.
\end{ex}

\begin{ex}[m]
  Sei $f\colon \mathbf C \to \mathbf C$ eine holomorphe Funktion, deren Realteil nach oben beschränkt 
  ist.  
  Zeige, daß $f$ konstant ist.
\end{ex}

\begin{ex}[s]
  Sei $f\colon X \to Y$ eine nicht konstante holomorphe Funktion zwischen Riemannschen Flächen.
  Zeige Sie, daß
  \[
    f^*\colon \mathcal O(Y) \to \mathcal O(X),\ \phi \mapsto \phi \circ f
  \]
  ein injektiver Morphismus von $\mathbf C$-Algebren ist.
\end{ex}

\chapter{Differentialformen}

\begin{ex}[s]
  Seien $X$ eine kompakte Riemannsche Fläche und $p_1$, \dots, $p_n$ Punkte auf $X$.
  Sei $X' \coloneqq X \setminus \{p_1, \dots, p_n\}$, und sei $f\colon X' \to \mathbf C$
  eine nicht-konstante holomorphe Funktion. Zeigen Sie, daß $f(X')$ dicht in $\mathbf C$ liegt.

  (Unterscheiden Sie die Fälle, daß sich $f$ zu einer meromorphen Funktion auf $X$ fortsetzt oder
  nicht.)
\end{ex}

\begin{ex}[m]
  Sei $p \colon \mathbf C \to \mathbf C^\times,\ z \mapsto e^z$, die \emph{universelle Überlagerung
  von $\mathbf C^\times$}. Sei die holomorphe $1$-Form
  \[
    \omega \coloneqq \frac{dz} z \in \Omega(\mathbf C^\times)
  \]
  gegeben. Berechnen Sie $p^* \omega$.
\end{ex}

\begin{ex}[m]
  Zeigen Sie, daß die holomorphe $1$-Form
  \[
    \frac{dz}{1 + z^2} \in \Omega(\mathbf C \setminus \{\pm i\})
  \]
  zu einer holomorphen $1$-Form $\omega$ auf $\mathbf P^1 \setminus \{\pm i\}$ fortgesetzt werden 
  kann. Sei $p\colon \mathbf C \to \mathbf P^1 \setminus \{\pm i\},\ z \mapsto \tan z$, und 
  berechnen Sie $p^* \omega$.

  (Überlegen Sie sich, warum es reicht, Holomorphie von Differentialformen nur mit Karten eines
  Atlas zu testen.)
\end{ex}

\begin{ex}[s]
  Sei $p\colon Y \to X$ eine holomorphe Abbildung zwischen Riemannschen Flächen. Seien $a \in X$,
  $b \in p^{-1}(\{a\})$ und $k \ge 1$ die Multiplizität von $p$ an $b$, das heißt,
  für jede offene Umgebung
  $V$ von $b$ existieren offene Umgebungen $W \subseteq V$ und $U$ von $b$ bzw.~$a$, so daß
  $p^{-1}(\{x\}) \cap W$ für $x \in U \setminus \{a\}$ jeweils genau $k$ Elemente enthält.

  Zeigen Sie, daß für eine holomorphe $1$-Form $\omega$ auf $X \setminus \{a\}$ dann gilt, daß 
  \[
    \operatorname{Res}_b (p^* \omega) = k \cdot \operatorname{Res}_a (\omega).
  \]
\end{ex}

\chapter{Integration von Differentialformen}

\begin{ex}
  Seien $X$ eine Riemannsche Fläche und $\omega$ eine holomorphe $1$"~Form auf
  $X$. Seien $a \in X$ und $\phi_a \in \mathcal O_a$ der Keim eines Potentials
  (d.h.~eines Primitiven) von $\omega$ in einer Umgebung von $a$.
  \begin{enumerate}
  \item(s)
    Sei $Y$ die Zusammenhangskomponente von $|\mathcal O|$ mit $\phi_a \in Y$.
    Zeigen Sie, daß die Einschränkung
    \[
      p\colon Y \to X
    \]
    eine Überlagerung ist.
  \item(s)
    Wir definieren eine Funktion durch
    \[
      f\colon Y \to \mathbf C,\ \eta \mapsto \eta(p(\eta)).
    \]
    (Beachten Sie, daß $\eta \in \mathcal O_{p(\eta)}$.)
    Zeigen Sie, daß $f$ holomorph ist und ein Potential von $p^* \omega$ ist.
  \item(m)
    Zeigen Sie, daß eine Untergruppe $G$ der
    (additiven) Gruppe von $\mathbf C$ 
    existiert, so daß für alle $\eta, \eta' \in Y$ mit $p(\eta) = p(\eta')$
    genau ein $c \in G$ mit $\eta' = \eta + c$ existiert.
  \end{enumerate}
\end{ex}

\begin{ex}[m]
  Sei $X \coloneqq \mathbf C/\Gamma$ ein Torus. Zeigen Sie, daß für jeden
  Homomorphismus
  \[
    a\colon H_1(X, \mathbf Z) \to \mathbf C
  \]
  eine geschlossene $1$-Form $\omega \in \mathcal E^1(X)$ existiert, deren 
  Periodenhomomorphismus gerade $a$ ist.

  (Dabei dürfen Sie die erste Homologiegruppe $H_1(X, \mathbf Z)$ als
  Abelisierung der Fundamentalgruppe $\pi_1(X)$ verstehen.)
\end{ex}

\begin{ex}[s]
  Seien $X$ eine Riemannsche Fläche und $\omega \in \mathcal M^1(X)$ ein 
  abelsches Differential zweiter Art auf $X$, das heißt eine meromorphe $1$-
  Form mit verschwindendem Residuum an jedem Pol. Zeigen Sie, daß eine
  Überlagerung 
  $p\colon Y \to X$ und eine meromorphe Funktion
  $F \in \mathcal M(Y)$ mit $d F = p^* \omega$ existieren.
\end{ex}

\begin{ex}[m]
  Sei $X \coloneqq \mathbf C/\Gamma$ ein Torus. Zeigen Sie mit Hilfe des
  Residuensatzes, daß
  keine meromorphe Funktion $f \in \mathcal M(X)$ existiert, deren einziger 
  Pol von Ordnung $1$ ist.
\end{ex}

\chapter{Kohomologiegruppen}

\begin{ex}[m]
  \leavevmode
  \begin{enumerate}
  \item
    Seien $X = U \cup V$ ein topologischer Raum sowie
    $U$ und $V$ offene, einfach zusammenhängende Teilräume in $X$.
    Weiter sei $U \cap V$ wegweise zusammenhängend. Zeigen Sie, daß
    dann $U \cup V$ einfach zusammenhängend ist.
  \item
    Folgern Sie, daß die Sphären $\mathbf S^n$ für $n \ge 2$ einfach
    zusammenhängend sind.
  \end{enumerate}
\end{ex}

\begin{ex}[s]
  Seien $p_1$, \dots, $p_n$ paarweise verschiedene Punkte von $\mathbf C$. Sei
  \[
    X \coloneqq \mathbf C \setminus \{p_1, \ldots, p_n\}.
  \]
  Zeigen Sie, daß $H^1(X, \mathbf Z) \simeq \mathbf Z^n$.

  (Konstruieren Sie eine offene Überdeckung $(U, V)$ von $X$, so daß $U$
  und $V$ einfach zusammenhängende Teilräume sind und
  $U \cap V$ insgesamt $n + 1$ Zusammenhangskomponenten hat.)
\end{ex}

\begin{ex}
  \label{ex:finite}
  \leavevmode
  \begin{enumerate}
  \item(m)
    Seien $X$ eine topologische Mannigfaltigkeit und $U$ eine offene Teilmenge
    von $X$. Sei $V$ eine in $U$ relativ kompakte offene Teilmenge von $X$,
    das heißt $V$ ist offen, $\overline V \subseteq U$ und $\overline V$ ist
    kompakt.

    Zeigen Sie, daß $V$ nur eine endliche Anzahl der Zusammenhangskomponenten
    von $U$ nicht trivial schneidet.
  \item(s)
    \label{it:covering}
    Sei $X$ eine kompakte topologische Mannigfaltigkeit. Zudem
    seien $\mathfrak U = (U_i)_{i \in I}$ und $\mathfrak V = (V_i)_{i \in I}$
    zwei endliche offene Überdeckungen von $X$, so daß $V_i$ für alle
    $i \in I$ relativ kompakt in $U_i$ ist. Zeigen Sie, daß das Bild von
    \[
      t^{\mathfrak U}_{\mathfrak V}\colon Z^1(\mathfrak U, \mathbf C)
      \to Z^1(\mathfrak V, \mathbf C)
    \]
    ein endlich-dimensionaler Vektorraum ist.
  \item(s)
    Sei $X$ eine kompakte Riemannsche Fläche. Zeigen Sie, daß
    $H^1(X, \mathbf C)$ ein endlich-dimensionaler Vektorraum ist.

    (Betrachten Sie endliche offene Überdeckungen $\mathfrak U$
    und $\mathfrak V$ von $X$ wie in~(\ref{it:covering}) und so daß
    $U_i$ und $V_i$ isomorph zu offenen Kreisscheiben sind.)
  \end{enumerate}
\end{ex}

\begin{ex}
  Sei $X$ eine kompakte Riemannsche Fläche.
  \begin{enumerate}
  \item(m)
    \label{it:injection}
    Zeigen Sie, daß die durch die Inklusion $\mathbf Z \to \mathbf C$
    induzierte Abbildung $H^1(X, \mathbf Z) \to H^1(X, \mathbf C)$
    injektiv ist.
  \item(s)
    Zeigen Sie, daß $H^1(X, \mathbf Z)$ eine endlich erzeugte freie abelsche
    Gruppe ist.

    (Ähnlich wie die endliche Dimensionalität in
    Aufgabe~\ref{ex:finite} läßt sich hier zeigen, daß $H^1(X, \mathbf Z)$ 
    endlich erzeugt ist. Aufgrund der in~(\ref{it:injection})
    gezeigten Einbettung in einen Vektorraum folgt dann, daß die Gruppe
    frei ist.)
  \end{enumerate}
\end{ex}

\chapter{Das Dolbeaultsche Lemma und $L^2$-Räume}

\begin{ex}[m]
  Sei $X \coloneqq \{z \in \mathbf C \mid |z| < R\}$ für $0 < R \leq \infty$. Sei $\mathcal H$ die
  Garbe der harmonischen Funktionen auf $X$, das heißt
  \[
    \mathcal H(U) = \{f\colon U \to \mathbf C\mid \text{$f$ ist harmonisch}\}
  \]
  für jedes offene $U$ in $X$. Zeigen Sie, daß $H^1(X, \mathcal H) = 0$.
\end{ex}

\begin{ex}[s]
  \leavevmode
  \begin{enumerate}
  \item
    Zeigen Sie, daß $\mathfrak U \coloneqq (U_1, U_2) \coloneqq
    (\mathbf P^1 \setminus \{\infty\}, \mathbf P^1 \setminus \{0\})$
    eine Leraysche Überdeckung für die Garbe $\Omega^1$ der holomorphen $1$-Formen auf $\mathbf P^1$
    ist.
  \item
    Zeigen Sie, daß $H^1(\mathbf P^1, \Omega) \simeq H^1(\mathfrak U, \Omega) \simeq \mathbf C$
    und daß die Kohomologieklasse von
    \[
      \frac{dz}{z} \in \Omega(U_1 \cap U_2) \simeq Z^1(\mathfrak U, \Omega)
    \]
    eine Basis von $H^1(\mathbf P^1, \Omega)$ ist.
  \end{enumerate}
\end{ex}

\begin{ex}[s]
  Sei $g \in \mathcal E(\mathbf C)$ eine Funktion mit kompaktem Träger. Zeigen Sie, daß genau dann
  eine Lösung $f \in \mathcal E(\mathbf C)$ der Gleichung $\frac{\partial f}{\partial \bar z} = g$
  mit kompaktem Träger existiert, wenn
  \[
    \iint_{\mathbf C} z^n g(z) dz \wedge d\bar z = 0
  \]
  für alle $n \in \mathbf N_0$.
\end{ex}

\begin{ex}[m]
  Sei $H$ ein Hilbertraum. Eine \emph{Hilbertbasis $(h_i)_{i \in I}$} von $H$ ist ein orthonormales
  System $(h_i)_{i \in I}$ von Vektoren aus $H$, so daß der topologische Abschluß des linearen
  Spanns  von $\{h_i \mid i \in I\}$ ganz $H$ ist.

  Seien $0 < r < R < \infty$ und $X \coloneqq \{z \in \mathbf C \mid r < |z| < R\}$ ein Kreisring.
  Finden sie eine Hilbertbasis $(\phi_n)_{n \in \mathbf Z}$ von $L^2(X, \mathcal O)$ von der Form
  \[
    \phi_n = c_n z^n
  \]
  mit $c_n \in \mathbf C$.
\end{ex}

\begin{ex}[s]
  Sei $X \subseteq \mathbf C$ beschränkt und offen. Seien
  $p_1, \dots, p_k \in X$ und $X' \coloneqq X \setminus \{p_1, \dotsc, p_k\}$. Zeigen Sie, daß die 
  Einschränkungsabbildung
  \[
    L^2(X, \mathcal O) \to L^2(X', \mathcal O)
  \]
  ein Isomorphismus ist.
\end{ex}

\chapter{Die exakte Kohomologiesequenz}

\begin{ex}[m]
  Sei $X$ eine Riemannsche Fläche. Sei $\mathcal H$ die Garbe der harmonischen
  Funktionen auf $X$. Zeigen Sie, daß die Sequenz
  \[
    \begin{CD}
      0 @>>> \mathcal H @>>> \mathcal E @>{d' d''}>> \mathcal E^2 @>>> 0
    \end{CD}
  \]
  eine kurze exakte Sequenz von Garben auf $X$ ist.
\end{ex} 

\begin{ex}[s]
  Sei $X$ eine Riemannsche Fläche. Ist $f \in \mathcal O^\times(U)$
  für ein offenes $U \subset X$ gegeben, so schreiben wir
  \[
    d \log f \coloneqq f^{-1} df
  \]
  für das \emph{logarithmische Differential} von $f$.

  Zeigen Sie, daß die Sequenz
  \[
    \begin{CD}
      0 @>>> \mathbf C^\times @>>> \mathcal O^\times @>{d \log}>> \Omega @>>> 0
    \end{CD}
  \]
  eine kurze exakte Sequenz von Garben auf $X$ ist.
\end{ex}

\begin{ex}[m]
  Sei $X$ eine Riemannsche Fläche. Sei $\mathcal Q$ die Untergarbe von
  $\mathcal M^{(1)}$ der meromorphen $1$-Formen mit verschwindendem Residuum
  an jedem Punkt. Zeigen Sie, daß die (wohldefinierte?) Sequenz
  \[
    \begin{CD}
      0 @>>> \mathbf C @>>> \mathcal M @>d>> \mathcal Q @>>> 0
    \end{CD}
  \]
  eine kurze exakte Sequenz von Garben auf $X$ ist.
\end{ex}

\begin{ex}[s]
  Sei $X = \mathbf C/\Gamma$ ein Torus. Es sei an den Isomorphismus
  \[
    H^1(X, \mathbf C) \simeq \mathrm{Rh}^1(X)
  \]
  erinnert. Zeigen Sie damit, daß $H^1(X, \mathbf C) \simeq \mathbf C^2$, in dem
  Sie zeigen, daß die Klassen von
  $dz$ und $d\bar z$ eine Basis von $\mathrm{Rh}^1(X)$ bilden.

  (Tip: Sei $\omega \in \mathcal E^{(1)}(X)$ geschlossen.
  Zeigen Sie, daß $c_1, c_2 \in \mathbf C$ existieren, so daß alle
  Perioden von $\omega - c_1 dz - c_2 d\bar z$ verschwinden.)
\end{ex}

\chapter{Der Riemann--Rochsche Satz}

\begin{ex}[s]
  Sei $D \in \mathrm{Div}(\mathbf P^1)$. Zeigen Sie, daß
  \[
    \dim H^0(\mathbf P^1, \mathcal O_D) = \max(0, 1 + \deg D)
  \]
  und
  \[
    \dim H^1(\mathbf P^1, \mathcal O_D) = \max(0, - 1 - \deg D).
  \]
\end{ex}

\begin{ex}[m]
  Sei $X = \mathbf C/\Gamma$ ein Torus. Sei $P \in X$. Zeigen Sie
  \[
    \dim H^0(X, \mathcal O_{n P}) = \begin{cases}
    0 & \text{für $n < 0$,} \\
    1 & \text{für $n = 0$ und} \\
    n & \text{für $n \ge 1$.}
    \end{cases}
  \]
  (Tip: Betrachten Sie die Weierstra\ss sche $\wp$-Funktion.)
\end{ex}

\begin{ex}[s]
  Seien $X$ eine kompakte Riemannsche Fläche, $D \in \mathrm{Div}(X)$
  und $\mathfrak U = (U_i)_{i \in I}$ eine offene Überdeckung von $X$, so daß jedes
  $U_i$ biholomorph zu einer Scheibe ist. Zeigen Sie, daß
  $\mathfrak U$ eine Leraysche Überdeckung für die Garbe $\mathcal O_D$ ist, das heißt also,
  daß $H^1(U_i, \mathcal O_D|_{U_i}) = 0$.
\end{ex}

\begin{ex}[s]
  \leavevmode
  \begin{enumerate}
  \item
    Sei $X$ eine Riemannsche Fläche. Für jede offene Teilmenge $U$ von $X$ sei
    \[
      \mathcal D(U) 
    \]
    die abelsche Gruppe aller formalen in $U$ lokal endlichen
    $\mathbf Z$-Li\-ne\-ar\-kom\-binationen von Punkten in $U$. Zeigen Sie, daß $\mathcal D$ mit den 
    natürlichen Einschränkungsabbildungen eine Garbe abelscher Gruppen auf $X$ definiert.
  \item
    Zeigen Sie, daß $H^1(X, \mathcal D) = 0$.

  (Tip: Imitieren Sie den Beweis der aus der Vorlesung bekannten Aussage
  $H^1(X, \mathcal E) = 0$. Dabei ist 
  dort verwendete Partition der Eins durch eine mit Werten in $\mathbf Z$ und ohne 
  Differenzierbarkeitsforderungen zu ersetzen.)
  \item
    Zeigen Sie, daß die Sequenz
    \[
      \begin{CD}
        0 @>>> \mathcal O^\times @>>> \mathcal M^\times @>>> \mathcal D @>>> 0
      \end{CD}
    \]
    eine exakte Sequenz von Garben abelscher Gruppen ist. Dabei ist der zweite nicht triviale Morphismus 
    derjenige, der jeder meromorphen Funktion ihren Hauptdivisor zuordnet.
  \item
    Zeigen Sie, daß eine natürliche lange exakte Sequenz abelscher Gruppen der Form
    \[
      \begin{CD}
        0 @>>> H^0(X, \mathcal O^\times) @>>> H^0(X, \mathcal M^\times) @>>> \mathrm{Div}(X) \\
        @>>> H^1(X, \mathcal O^\times) @>>> H^1(X, \mathcal M^\times) @>>> 0
      \end{CD}
    \]
    existiert.
  \end{enumerate}
\end{ex}

\chapter{Serresche Dualität}

\begin{ex}[m]
  Sei $\pi\colon X \to \mathbf P^1$ die Riemannsche Fläche zur algebraischen
  Funktion $\sqrt[n]{1 - z^n}$, das heißt
  zu $P \coloneqq T^n + z^n - 1 \in \mathcal M(\mathbf P^1)[T]$, wobei $z \in \mathcal M(\mathbf P^1)$
  die kanonische Koordinatenfunktion ist.

  Zeigen Sie mittels der Riemann--Hurwitz-Formel, daß für das Geschlecht $g$ von $X$ gilt:
  \[
    g = \frac{(n - 1)(n - 2)} 2.
  \]
\end{ex}

\begin{ex}[m]
  Sei $X$ eine kompakte Riemannsche Fläche. Sei $\mathcal Q(X)$ der Vektorraum der
  globalen meromorphen $1$-Formen auf $X$, deren Residuen an jeder Stelle verschwinden.
  Zeigen Sie, daß ein natürlicher Isomorphismus
  \[
    H^1(X, \mathbf C) \simeq \mathcal Q(X)/d \mathcal M(X)
  \]
  von Vektorräumen existiert.
  (Tip: Nach einer vorausgegangen Übung existiert eine exakte Sequenz der Form
  $0 \to \mathbf C \to    
  \mathcal M \to \mathcal Q \to 0$.)
\end{ex}

\begin{ex}[s]
  Sei $X = \mathbf C/\Gamma$ ein Torus.
  Zeigen Sie, daß die Klassen von $dz$ und $\wp_\Gamma dz$ eine Basis von
  $\mathcal Q(X)/d \mathcal M(X)$ bilden.
\end{ex}

\begin{ex}[s]
  Sei $X$ eine kompakte Riemannsche Fläche und $D$ ein Divisor auf $X$. Zeigen Sie:
  \begin{align*}
    \dim H^0(X, \mathcal O_D) = 0 &&& \text{für $\deg D \le -1$} \\
    0 \le \dim H^0(X, \mathcal O_D) \leq 1 + \deg D &&& \text{für $-1 \le \deg D \le g - 1$} \\
    1 - g + \deg D \leq \dim H^0(X, \mathcal O_D) \leq g
    &&& \text{für $g - 1 \leq \deg D \leq 2 g - 1$} \\
    \dim H^0(X, \mathcal O_D) = 1 - g + \deg D &&& \text{für $\deg D \ge 2 g - 1$}.
  \end{align*}
\end{ex}

\begin{ex}[m]
  Seien $K$ ein kanonischer Divisor auf einer kompakten Riemannschen Fläche $X$
  von einem Geschlecht $g > 0$. Sei weiter $D \ge K$ ein Divisor mit $\deg D
  = \deg K + 1$. Zeigen Sie, daß $\mathcal O_K$ global erzeugt ist, aber
  $\mathcal O_D$ nicht.
\end{ex}

\begin{ex}[s]
  Sei $X$ eine kompakte Riemannsche Fläche vom Geschlecht $g=2$. Mögen
  $\omega_1$ und $\omega_2$ eine Basis von $H^0(X, \Omega)$ bilden. Definieren
  Sie $f \in \mathcal M(X)$ durch $\omega_1 = f \omega_2$. Zeigen Sie, daß
  $f\colon X \to \mathbf P^1$ eine zweifache (verzweigte) Überlagerung ist.
\end{ex}

\chapter{Funktionen mit vorgeschriebenen Hauptteilen}

\begin{ex}[m]
  Sei $U \coloneqq \{z \in \mathbf C: |z| < r\}$ mit $r > 0$ eine Kreisscheibe. Sei weiter
  $f\colon U \to \mathbf C$ eine holomorphe Funktion mit $f(0) \neq 0$.
  \begin{enumerate}
  \item
    Sei $f_j(z) \coloneqq z^{j - 1} f(z)$ für $j = 1$, \dots, $g$. Zeigen Sie, daß die
    Wrońskische $W(f_1, \dotsc, f_g)$ am Ursprung nicht verschwindet.
  \item
    Sei $\phi_j(z) \coloneqq z^{2j - 2} f(z)$ für $j = 1$, \dots, $g$. Zeigen Sie, daß die
    Wrońskische $W(\phi_1, \dotsc, \phi_g)$ am Ursprung von Ordnung $g (g - 1)/2$ verschwindet.
  \end{enumerate}
\end{ex}

\begin{ex}[s]
  Sei $\pi\colon X \to \mathbf P^1$ eine hyperelliptische Riemannsche Fläche
  vom Geschlecht $g \ge 2$.
  \begin{enumerate}
  \item
    Zeigen Sie, daß alle Verzweigungspunkte $p_1$, \dots, $p_{2g + 2}$ (?) von $\pi$
    Weierstraßpunkte auf $X$ sind.
  \item
    Zeigen Sie, daß keine weiteren Weierstraßpunkte existieren und daß jeder Weierstraßpunkt vom
    Gewicht $g(g - 1)/2$ ist.
  \end{enumerate}
\end{ex}

\begin{ex}[s]
  Sei $X$ eine kompakte Riemannsche Fläche vom Geschlecht $g$. Sei $D$ ein Divisor auf $X$ mit
  $D \ge 0$. Sei $M_D$ die Menge aller Mittag--Leffler-Verteilungen $\mu \ge -D$ auf $X$, das
  heißt die Menge aller Mittag--Leffler-Verteilungen, die in $C^0(\mathfrak U, \mathcal O_D)$
  für eine offene Überdeckung $\mathfrak U$ von $X$ liegen. Wir definieren eine lineare (?) Abbildung
  \[
    R\colon M_D \to \Omega(X)\spcheck
  \]
  durch
  \[
    R(\mu)(\omega) = \operatorname{Res}(\mu \omega)
  \]
  für $\mu \in M_D$ und $\omega \in \Omega(X)$.
  Zeigen Sie, daß
  \[
    \dim H^1(X, \mathcal O_D) = g - \operatorname{rk} R.
  \]
\end{ex}

\chapter{Harmonische Differentialformen}

\begin{ex}[s]
  Sei $X$ eine kompakte Riemannsche Fläche.
  \begin{enumerate}
  \item
    Zeigen Sie, daß $d \mathcal E^{0, 1}(X) = d' d'' \mathcal E(X) \subseteq \mathcal E^{(2)}(X)$.
  \item
    Sei $\mathcal H$ die Garbe der harmonischen Funktionen auf $X$. Geben Sie natürliche Isomorphismen
    \[
      H^1(X, \mathcal H) \simeq \mathcal E^{(2)}(X)/d' d'' \mathcal E(X) \simeq \mathbf C
    \]
    an.
  \item
    Sei $\omega \in \mathcal E^{(2)}(X)$. Zeigen Sie, daß genau dann eine Funktion
    $f \in \mathcal E(X)$ mit $d' d'' f = \omega$ existiert, wenn $\iint_X \omega = 0$.
  \end{enumerate}
\end{ex}

\begin{ex}
  Sei $X \coloneqq \mathbf C/\Gamma$ ein Torus. Für eine Funktion $f \in \mathcal E(X)$ sei ihr
  \emph{Mittelwert} durch
  \[
    M(f) \coloneqq \Biggl(\iint_X f dz \wedge d\bar z\Biggr)
    \Biggl(\iint_X dz \wedge d\bar z\Biggr)^{-1}
  \]
  definiert.
  Für $\omega = f dz + g d\bar z \in \mathcal E^{(1)}(X)$ sei $M(\omega) \coloneqq
  M(f) dz + M(g) d\bar z$.
  \begin{enumerate}
  \item(s)
    Sei $\omega \in \mathcal Z(X) \coloneqq \operatorname{ker}(d\colon \mathcal E^{(1)}(X) \to
    \mathcal E^{(2)}(X))$. Zeigen Sie, daß dann $\omega$ und $M(\omega)$ kohomolog sind, sich also
    nur um eine exakte $1$-Form unterscheiden.
  \item(m)
    Zeigen Sie, daß die wohldefinierte (?) Abbildung
    \[
      M\colon \mathcal Z(X) \to \mathrm{Harm}^1(X)
    \]
    einen Isomorphismus
    \[
      \mathrm{Rh}^1(X) \simeq \mathrm{Harm}^1(X)
    \]
    induziert.
  \end{enumerate}
\end{ex}

\begin{ex}[s]
  Bezeichne $X$ eine kompakte Riemannsche Fläche. Für eine geschlossene Kurve $\alpha \in Z_1(X, \mathbf Z)$
  bezeichnen wir mit $\sigma_\alpha \in \mathrm{Harm}^1(X)$ diejenige harmonische
  $1$-Form, für die
  \[
    \oint_\alpha \omega = \iint_X \sigma_\alpha \wedge \omega
  \]
  für alle geschlossenen $1$-Formen $\omega \in \mathcal Z(X)$ gilt.

  Seien $\alpha, \beta \in Z_1(X, \mathbf Z)$. Zeigen Sie, daß die \emph{Schnittzahl}
  \[
    \iint_X \sigma_\alpha \wedge \sigma_\beta
  \]
  von $\alpha$ und $\beta$ eine ganze Zahl ist.

  (Tip: Zeigen Sie zunächst, daß
  $\frac 1 {2 \pi i} \oint_\alpha d \log f$ für $f \in \mathcal E(X)$ eine ganze Zahl ist.)
\end{ex}

\begin{ex}[m]
  Seien $\Gamma = \mathbf Z \gamma_1 + \mathbf Z \gamma_2 \subseteq \mathbf 
  C$ ein Gitter und $X \coloneqq \mathbf C/\Gamma$ der zugehörige Torus.
  Sei $\pi\colon \mathbf C \to X$ die natürliche Projektion. Wir definieren
  \[
    \alpha_i\colon [0, 1] \to X, t \mapsto \pi(t \gamma_i)
  \]
  für $i = 1$, $2$.
  Geben Sie die harmonischen $1$-Formen $\sigma_{\alpha_1}$ und $\sigma_{\alpha_2}$ an.
\end{ex}

\chapter{Der Abelsche Satz und das Jacobische Umkehrproblem}

\begin{ex}[s]
  Bezeichne $X$ eine kompakte Riemannsche Fläche vom Geschlecht~$g$. Zeigen Sie,
  daß $\alpha_1$, \dots, $\alpha_{2g} \in Z_1(X, \mathbf Z)$ mit
  \[
    \mathrm{Harm}^1(X) = \sum_{i = 1}^{2g} \mathbf C \sigma_{\alpha_i}
  \]
  existieren.
\end{ex}

\begin{ex}
  Sei $\Gamma \subseteq \mathbf C$ ein Gitter.
  Eine \emph{Theta-Funktion bezüglich $\Gamma$} ist eine holomorphe Funktion
  $F\colon \mathbf C \to \mathbf C$ mit
  \[
    \forall z \in \mathbf C, \gamma \in \Gamma: F(z + \gamma) = \exp(L_\gamma(z)) \cdot F(z),
  \]
  wobei eine $L_\gamma(z) = a_\gamma z + b_\gamma$ eine von $\gamma$ abhängige affine Funktion ist.
  \begin{enumerate}
  \item(s)
    Die \emph{Weierstraßsche $\sigma$-Funktion bezüglich $\gamma$} ist durch
    \[
      \sigma\colon \mathbf C \to \mathbf C, z \mapsto z \cdot 
      \prod_{\gamma \in \Gamma \setminus \{0\}} \Biggl(\Bigl(1 - \frac z \gamma\Bigr) \cdot
      \exp\Bigl(\frac z \gamma + \frac{z^2} {2 \gamma^2}\Bigr)\Biggr)
    \]
    definiert. Zeigen Sie, daß $\sigma$ eine Theta-Funktion ist,
    deren Nullstellen von erster Ordnung sind und genau auf den Gitterpunkten
    $\Gamma$ liegen.
  \item(m)
    Zeigen Sie, daß jede bezüglich $\Gamma$ doppelt-periodische meromorphe
    Funktion als ein Quotient zweier Theta-Funktionen dargestellt werden kann.
  \end{enumerate}
\end{ex}

\begin{ex}[m]
  Sei $\Gamma \subseteq \mathbf C$ ein Gitter und $\wp$ die zu $\Gamma$ gehörende
  Weierstraßsche $\wp$-Funktion. Zeigen Sie, daß
  \[
    (1 : \wp : \wp'): \mathbf C/\Gamma \to \mathbf P^2
  \]
  eine Einbettung ist, wenn $\wp$ und $\wp'$ als meromorphe Funktionen auf
  dem Torus $\mathbf C/\Gamma$ angesehen werden.
\end{ex}
 
\begin{ex}[s]
  Sei $X$ eine kompakte Riemannsche Fläche und $Y \subseteq X$ eine offene Teilmenge,
  welche nicht dicht ist, das heißt also $X \setminus Y$ hat ein nicht leeres Inneres.
  Sei $D$ ein Divisor auf $X$. Zeigen Sie, daß eine meromorphe Funktion
  $f \in \mathcal M^\times(X)$ mit
  \[
    \bigl(f|_Y\bigr) = D|_Y
  \]
  existiert.

  (Tip: Finden Sie einen Divisor $D'$ mit $\operatorname{supp} D' \subseteq X \setminus Y$,
  so daß $D + D'$ ein Hauptdivisor ist.)
\end{ex}

\chapter{Die Jacobi Varietät}

\begin{ex}
  \leavevmode
  \begin{enumerate}
  \item(s)
    Seien $g_2, g_3 \in \mathbf C$. Zeigen Sie, daß das Polynom
    \[
      4 z^3 - g_2 z - g_3
    \]
    genau dann drei verschiedene Nullstellen in $z$ hat,
    wenn $g_2^3 - 27 g_3^2 \neq 0$.
  \item(m)
    Sei $\Gamma \subseteq \mathbf C$ ein Gitter und
    $\wp$ die zu $\Gamma$ gehörende Weierstaßsche $\wp$-Funktion. Seien
    \begin{align*}
      g_2 & \coloneqq 60 \sum_{\omega \in \Gamma \setminus \{0\}} \frac 1 {\omega^4}
      & & \text{und}
      & 
      g_3 & \coloneqq 140 \sum_{\omega \in \Gamma \setminus \{0\}} \frac 1 {\omega^6}.
    \end{align*}

    Zeigen Sie, daß $\wp$ der Differentialgleichung
    \[
      \wp^{\prime 2} = 4 \wp^3 - g_2 \wp - g_3
    \]
    genügt, und daß der Torus $\mathbf C/\Gamma$ isomorph zur Riemannschen Fläche
    $X \to \mathbf P^1$ der algebraischen Funktion
    \[
      \sqrt{4 z^3 - g_2 z - g_3}
    \]
    ist.
  \item(s)
    Seien $g_2, g_3 \in \mathbf C$ mit $g_2^3 - 27 g_3^2 \neq 0$.
    Zeigen Sie, daß ein Gitter $\Gamma \subseteq \mathbf C$ mit
    \begin{align*}
      g_2 & = 60 \sum_{\omega \in \Gamma \setminus \{0\}} \frac 1 {\omega^4}
      & & \text{und}
      &
      g_3 & = 140 \sum_{\omega \in \Gamma \setminus \{0\}} \frac 1 {\omega^6}.
    \end{align*}
    existiert.

    (Tip: Nutzen Sie die vorhergehende Teilaufgabe und die Tatsache, daß Riemannsche Flächen
    $X$ vom  Geschlecht $1$ isomorph zu ihrer eigenen Jacobischen sind.)
  \end{enumerate}
\end{ex}

\begin{ex}[s]
  Seien $X$ und $Y$ zwei kompakte Riemannsche Flächen. Seien weiter
  $A$ eine diskrete Teilmenge von $X$ und $B$ eine diskrete Teilmenge von $Y$.
  Seien $X' \coloneqq X \setminus A$ und $Y' \coloneqq Y \setminus B$. Zeigen Sie, daß sich jeder
  Isomorphismus $f'\colon X' \to Y'$ eindeutig zu einem Isomorphismus
  $f\colon X \to Y$ fortsetzt.
\end{ex}

\chapter{Algebraische Funktionen}

\begin{ex}[s]
  Seien $X$ und $Y$ zwei kompakte Riemannsche Flächen,
  deren Funktionenkörper $\mathcal M(X)$ und $\mathcal M(Y)$ isomorph sind.
  Zeigen Sie, daß dann auch $X$ und $Y$ isomorph sind.
\end{ex}

\begin{ex}[s]
  Sei $F(z,w)\coloneqq w^2-z^3w+z \in \mathbf C \{\!\{z\}\!\}[w]$.
  \begin{enumerate}
  \item
    Zeigen Sie, dass $F$ irreduzibel ist über $\mathbf C \{\!\{z\}\!\}$.
  \item
    Bestimmen Sie die Puiseux Entwicklung
    \[
    w = \sum_{n=0}^\infty c_nz^{n/2}
    \]
    der algebraischen Funktion definiert durch $F(z,w)=0$.
  \end{enumerate}
\end{ex}

\end{document}
